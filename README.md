## qssi-user 11 RKQ1.210327.001 V12.5.12.0.RKRJPKD release-keys
- Manufacturer: xiaomi
- Platform: holi
- Codename: iris
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.210327.001
- Incremental: V12.5.12.0.RKRJPKD
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: ja-JP
- Screen Density: 440
- Fingerprint: Redmi/iris/iris:11/RKQ1.210327.001/V12.5.12.0.RKRJPKD:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.210327.001-V12.5.12.0.RKRJPKD-release-keys
- Repo: redmi_iris_dump_31119


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
